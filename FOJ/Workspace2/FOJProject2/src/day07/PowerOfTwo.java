package day07;

public class PowerOfTwo {

	
	
	    public static int power(int num, int pow){
	    	int result = 1;
	    	for(int i=0; i<pow;i++){
	    		result *= num;
	    	}
	    	return result;
	    }
		public static void main(String[] args) {
			System.out.println(power(2, 3));
			System.out.println(power(3, 4));
			System.out.println(power(4, 5));
		}

	}
