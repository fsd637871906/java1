package day09;

public class Demo5 {
	public static void findRowSum(int arr[][]){
		int sum;
		
		for(int i = 0;i < 3; i++){
			sum = 0;
			for( int j = 0;j <3 ;j++){
				sum += arr[i][j];			
				}
			System.out.println("Row-"+(i+1)+"sum = "+sum);
			
		}
		System.out.println(" ");
	}

	public static void main(String[] args) {
		int arr[][] = new int[][]
				{{10,20,30},{40,50,60},{70,80,90}};
		int arr1[][] = new int[][]
						{{11,21,31},{41,51,61},{71,81,91}};
		int arr2[][] = new int[][]
								{{12,22,32},{42,52,62},{72,82,92}};
								
        findRowSum(arr);
		findRowSum(arr1);
		findRowSum(arr2);

	}

}
