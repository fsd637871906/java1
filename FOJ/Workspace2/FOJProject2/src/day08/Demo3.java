package day08;

public class Demo3 {
	public static int searchArry(int arr[],int num){
		
		for(int i =0;i<arr.length;i++){
			if(arr[i] == num){
				return i;
			}
		}
		
		
		return -1;
		
		
	}

	public static void main(String[] args) {
		int arr[] ={30,10,50,20,40};
		
		System.out.println(searchArry(arr ,99));
		System.out.println(searchArry(arr ,10));
		System.out.println(searchArry(arr ,20));
		System.out.println(searchArry(arr ,30));

	}

}
