package day08;

public class Demo2 {
	public static int[] sortAssending(int arr[]){
		int temp =0;

		for(int i = 0;i <arr.length-1;i++){
			for(int j =0;j<arr.length-1-i;j++){
				if(arr[j]>arr[j+1]){
					temp =arr[j];
					arr[j] =arr[j+1];
					arr[j+1] =temp;
				}
			}
		}
		return arr;
	}

	public static void main(String[] args) {
		int arr[] ={30,10,50,20,40};

		System.out.print("Before sorting");
		for(int i =0;i<arr.length;i++){
			System.out.println(arr[i]+ " ");
		}
		System.out.println("\n");

		arr = sortAssending(arr);

		System.out.println("After sorting");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}


	}

}
