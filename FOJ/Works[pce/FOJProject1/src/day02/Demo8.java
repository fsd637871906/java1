package day02;

public class Demo8 {

	public static void main(String[] args) {
		String s = "25";
		byte b = Byte.parseByte(s);
		short sh = Short.parseShort(s);
		int i = Integer.parseInt(s);
		long l= Long.parseLong(s);
		
		//lower to higher
		System.out.println("String s : " + s);
		System.out.println("Byte b : " + b);
		System.out.println("Short sh : " + sh);
		System.out.println("int i : " + i);
		System.out.println("long l: "+l);
		
		l  = 45;
		i  =  (int) l;
		sh =(short) i;
		b  = (byte) sh;
		
		s = b + "";
		
		//Higher to lower
		System.out.println("long l   : " + s);
		System.out.println("int i    : " + s);
		System.out.println("Short sh : " + s);
		System.out.println("Byte b   : " + s);
		System.out.println("string s : " + s);
		System.out.println();
		

	}

}
