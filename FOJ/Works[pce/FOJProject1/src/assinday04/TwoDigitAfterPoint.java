package assinday04;

//package day04Assignment;
//
//import java.text.DecimalFormat;
//
//public class TwoDigitAfterPoint {
//
//	public static void main(String[] args) {
//		
//		double number = 12.333333333 ;
//		DecimalFormat digit = new DecimalFormat("#.##");//decimalformat can format numbers in variety of ways
//		String rounded = digit.format(number);
//		
//		System.out.println(rounded);
//
//	}
//
//}
//package day04Assignment;
//
//public class TwoDigitAfterPoint {
//
//  public static void main(String[] args) {
//      
//      double number = 12.333333333;
//      String rounded = String.format("%.2f", number);
//      
//      System.out.println(rounded);
//
//  }
//
//}


public class TwoDigitAfterPoint {

  public static void main(String[] args) {
      
      double number = 12.333333333;
      
    
      double rounded = Math.round(number * 100.0) / 100.0;
      
      System.out.println(rounded);

    }

}

