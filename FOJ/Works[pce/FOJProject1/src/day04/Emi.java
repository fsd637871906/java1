package day04;

public class Emi {
	 public static double calculateEMI(double principal, double downPayment, double annualInterestRate, int loanTermMonths) {
	       
	        double loanAmount = principal - downPayment;
	        
	        double monthlyInterestRate = annualInterestRate / 12 / 100;
	        
	        double emi = loanAmount * monthlyInterestRate * Math.pow((1 + monthlyInterestRate), loanTermMonths) / (Math.pow((1 + monthlyInterestRate), loanTermMonths) - 1);
	        
	        return emi;
	 }
	public static void main(String[] args) {
		double carPrice = 850000; 
        double downPayment = 150000;  // Down payment in rupees
        double interestRate = 12;  // Annual interest rate (in percentage)
        int loanTermMonths = 48;  // Loan term in months
        
        // Calculating EMI
        double emi = calculateEMI(carPrice, downPayment, interestRate, loanTermMonths);
        
        System.out.println("EMI: " + emi + " rupees per month");
	}
		

	}


