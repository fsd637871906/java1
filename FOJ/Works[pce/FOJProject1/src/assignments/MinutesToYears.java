package assignments;

public class MinutesToYears {

	public static void main(String[] args) {
		
		
		int minutes = 60000;
		
		int years = minutes / (60 * 24 *365);
		
		System.out.println(minutes + " minutes is about " + years + " years ");
	}

}
