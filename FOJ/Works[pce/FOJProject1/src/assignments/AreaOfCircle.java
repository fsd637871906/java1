package assignments;



public class AreaOfCircle {

	public static void main(String[] args) {
		
		double radius = 5;
		double area = Math.PI * radius * radius;
		
		System.out.println("The area of a circle with radius " + radius + "is:"+area);
	}

}