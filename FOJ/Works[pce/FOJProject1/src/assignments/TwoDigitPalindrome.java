package assignments;

public class TwoDigitPalindrome {

	public static void main(String[] args) {
		

		int number = 121;

		int lastDigit = number % 10;
		int firstDigit = number / 10;

		System.out.println(number + (lastDigit == firstDigit ? " is a two-digit Palindrome. " : " is not a two digit Palindrome."));
	}

}
