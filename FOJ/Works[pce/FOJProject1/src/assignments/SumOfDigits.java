package assignments;


public class SumOfDigits {

	public static void main(String[] args) {
		
		
		int number = 347;
		
		int sum = (number / 100) + (number % 100) / 10 + (number % 10) ;
		
		System.out.println("The sum of the Digits in " + number + " is: " +sum);
	}

}
