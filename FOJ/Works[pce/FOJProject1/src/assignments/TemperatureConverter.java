package assignments;

public class TemperatureConverter {

	public static void main(String[] args) {
		
		double celsius = 25.0;
		double fahrenheit = 77.0;
		
		
		double fahrenheitFromCelsius = (celsius * 9.0 / 5.0) + 32.0;
		
		
		
		double celsiusFromFahrenheit = (fahrenheit -32.0) * 5.0 / 9.0;
		
		System.out.println(celsius + "Degrees Celsius is equal to " + fahrenheitFromCelsius + "Degrees Fahrenheit");
		System.out.println(celsius + "Degrees Fahrenheit is equal to " + celsiusFromFahrenheit + "Degrees Celsius");
	}

}

