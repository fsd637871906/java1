package assignments;



public class FizzBuzz {
	public static void main(String[] args) {

		int number = 15; 

		if (number % 15 == 0) {
			System.out.println("FizzBuzz");
		} else if (number % 3 == 0) {
			System.out.println("Fizz");
		} else if (number % 5 == 0) {
			System.out.println("Bizz");
		} else {
			System.out.println(number);
		}
	}
}