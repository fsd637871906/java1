package assignments;

public class AreaOfTriangle {

	public static void main(String[] args) {
		
		double base = 10;
		double height = 5;
		
		double area = 0.5 * base * height;
		
		System.out.println("Area of the triangle " + area + "square units");
		
	}

}
